import argparse;
import sys;
import time;
import httplib, urllib;
import RPi.GPIO as GPIO


#Parse the arguments passed into the launch program
parser = argparse.ArgumentParser(description='Skittle Game')
parser.add_argument('-s','--skittles', help='Number of skittles to read for', type=int, default=3, required=False)
args = vars(parser.parse_args())

#These are python LISTS
skittlesDown = [0,0,0,0,0];
lastSkittlesDown = [0,0,0,0,0];

#Reset the score
global score;
score = 0;

global scoreList 
scoreList = [5,3,1,2,4];
#Launch the skittles game

print "======================";
print "Launching skittle game";
print "======================";


#Read the input from GPIO pins

start_file = open("gpio.txt", "r")
skittlesDown[0] = int(start_file.readline());
skittlesDown[1] = int(start_file.readline());
skittlesDown[2] = int(start_file.readline());
skittlesDown[3] = int(start_file.readline());
skittlesDown[4] = int(start_file.readline());
start_file.close()

start_file.close()


#Check all the skittles are up
print "STARTUP SkittlesDown -->{}{}{}{}{}".format(skittlesDown[0],skittlesDown[1],skittlesDown[2],skittlesDown[3],skittlesDown[4]);

if (int(skittlesDown[0]) + int(skittlesDown[1]) + int(skittlesDown[2]) + int(skittlesDown[3]) + int(skittlesDown[4])) > 0:
	print "Please check that all skittles are up";
	for i in range (0,4):
		if (int(skittlesDown[i]) == 1):
			print "Skittle({}) appears to be down".format(i);
	
	print "Program Terminating";
	print "============== BYE ====================="
	sys.exit();


#Read the number of skittles that is sent into the launch command
if args['skittles'] >0:
	gameSkittles = args['skittles'];

remainingSkittles = gameSkittles;



def send_push(score):

	conn = httplib.HTTPSConnection("api.pushover.net:443")
	conn.request("POST", "/1/messages.json",
	  urllib.urlencode({
	    "token": "aV94h18eEQ82BwhVyc7F6XRkZTPDEu",
	    "user": "ukh6Z3nqMfSs7K1wUg2YDSmbN9rdXw",
	    "message": "You scored {} points".format(score),
	    "title" : "Skittles"
	  }), { "Content-type": "application/x-www-form-urlencoded" })
	conn.getresponse()



def check_skittle(skittleIndex, remainingSkittles, skittlesDown, lastSkittlesDown):
	if int(skittlesDown[skittleIndex]) != int(lastSkittlesDown[skittleIndex]):
		print "SkittlesDown -->{}{}{}{}{}".format(skittlesDown[0],skittlesDown[1],skittlesDown[2],skittlesDown[3],skittlesDown[4]);
		print "lastSkittlesDown -->{}{}{}{}{}".format(lastSkittlesDown[0],lastSkittlesDown[1],lastSkittlesDown[2],lastSkittlesDown[3],lastSkittlesDown[4]);
		print"Skittle({}) has been downed".format(skittleIndex);
		#Increment the score and decrease the remaining skittle counter
		global score
		score = score + scoreList[skittleIndex]
		print "You just scored {} bringing your total to {}".format(scoreList[skittleIndex],score);
		lastSkittlesDown[skittleIndex] = 1;
		remainingSkittles = remainingSkittles -1;
		print "{}{}{}".format("You have ", remainingSkittles, " remaining.....");
	return remainingSkittles;




print "{}{}{}".format("The game has been configured with ", gameSkittles, " skittles per game");


#Read the skittle inputs

print "{}{}{}".format("You have ", remainingSkittles, " remaining.....");


while remainingSkittles>0:
	
	#Read the input from GPIO pins	
	text_file = open("gpio.txt", "r")
	skittlesDown[0] = text_file.readline();
	skittlesDown[1] = text_file.readline();
	skittlesDown[2] = text_file.readline();
	skittlesDown[3] = text_file.readline();
	skittlesDown[4] = text_file.readline();
	text_file.close()
	time.sleep(2);



	# Check if the downed skittles array has changed

	remainingSkittles = check_skittle(0, remainingSkittles, skittlesDown, lastSkittlesDown);	
	remainingSkittles = check_skittle(1, remainingSkittles, skittlesDown, lastSkittlesDown);	
	remainingSkittles = check_skittle(2, remainingSkittles, skittlesDown, lastSkittlesDown);	
	remainingSkittles = check_skittle(3, remainingSkittles, skittlesDown, lastSkittlesDown);	
	remainingSkittles = check_skittle(4, remainingSkittles, skittlesDown, lastSkittlesDown);	
#End of while loop to read the skittle input



print "Last skittle has been knocked over"
#Skittle is the last in a frame

#Increment the score

#Report the total score
print "Congratulations - You scored {} points".format(score);
send_push(score)


#Reset the game




